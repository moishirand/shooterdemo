﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    public float acceleration;
    public float limitSpeed;

    private Rigidbody _rb;
    private KeyCode[] _inputs;
    private Vector3[] _directions;

    void Start () {
        _inputs = new KeyCode[] { KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D };
        _directions = new Vector3[] { Vector3.forward, Vector3.left, Vector3.back, Vector3.right };
        _rb = GetComponent<Rigidbody>();
    }
	
	void Update () {
        for (int i = 0; i < _inputs.Length; i++)
        {
            var key = _inputs[i];

            if (Input.GetKey(key))
            {
                Vector3 movement = _directions[i] * acceleration * Time.deltaTime;
                MovingPlayer(movement);
            }
        }
    }

    void MovingPlayer(Vector3 movement)
    {
        if (_rb.velocity.magnitude * acceleration > limitSpeed)
        {
            _rb.AddForce(movement * -1);
        }
        else
        {
            _rb.AddForce(movement);
        }
    }
}
