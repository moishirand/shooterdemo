﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public EnemyCreator enemyCreator;
    public GameObject playerPrefab;

    void Start()
    {
        var player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.onPlayerDied += OnPlayerDied;
    }

    void OnPlayerDied(Player player)
    {
        enemyCreator.SpawnEnemies(false);
        Destroy(player.gameObject);

        Invoke("RestartGame", 3);
    }

    void RestartGame()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in enemies)
        {
            Destroy(enemy);
        }

        var playerObject = Instantiate(playerPrefab, new Vector3(0, 0.5f, 0), Quaternion.identity);
        var cameraRig = Camera.main.GetComponent<CameraMove>();
        cameraRig.target = playerObject;
        enemyCreator.SpawnEnemies(true);
        playerObject.GetComponent<Player>().onPlayerDied += OnPlayerDied;
    }
}
