﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public int damage;

    private Vector3 direction;

    void Update()
    {
        this.transform.Translate(direction * speed, Space.World);
    }

    public void Fire(Ray shootRay)
    {
        this.direction = shootRay.direction;
        this.transform.position = shootRay.origin;
        RotateInDirection();
    }

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.CompareTag("Enemy"))
        {
            col.gameObject.GetComponent<Enemy>().TakeDamage(damage);
        }

        Destroy(this.gameObject);
    }

    void RotateInDirection()
    {
        Vector3 rotate = Vector3.RotateTowards(transform.forward, direction, 0.01f, 0.0f);
        transform.rotation = Quaternion.LookRotation(rotate);
    }
}
