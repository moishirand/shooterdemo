﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public float moveSpeed;
    public GameObject target;

    private Vector3 _offset;

     void Start()
    {
        _offset = transform.position - target.transform.position;
    }


     void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        transform.position = Vector3.Lerp(transform.position, target.transform.position + _offset, Time.deltaTime * moveSpeed);

    }
}
