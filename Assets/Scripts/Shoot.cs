﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Bullet bulletPrefab;
    public LayerMask mask;

    void Update()
    {
        bool mouseButtonDown = Input.GetMouseButtonDown(0);
        if (mouseButtonDown)
        {
            RaycastOnMouseClick();
        }
    }

    void RaycastOnMouseClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 100.1f, Color.red, 2);

        if (Physics.Raycast(ray, out hit, 100.0f, mask))
        {
            ShootBullet(hit);
        }
    }

    void ShootBullet(RaycastHit hit)
    {
        var bullet = Instantiate(bulletPrefab).GetComponent<Bullet>();
        var point = hit.point + new Vector3(0, this.transform.position.y, 0);
        var direction = point - transform.position;
        var shootRay = new Ray(this.transform.position, direction);
        Debug.DrawRay(shootRay.origin, shootRay.direction * 100.1f, Color.green, 2);

        //another option use physics system
        Physics.IgnoreCollision(GetComponent<Collider>(), bullet.GetComponent<Collider>());
        bullet.Fire(shootRay);

        //Be sure to destroy the bullets after second time
        Destroy(bullet.gameObject, 3);
    }
}
