﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    public int health = 3;

    public event Action<Player> onPlayerDied;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            CollidedWithEnemy(col.gameObject.GetComponent<Enemy>());
        }
    }

    void CollidedWithEnemy(Enemy enemy)
    {
        enemy.Attack(this);
        if (health <= 0)
        {
            if (onPlayerDied != null)
            {
                onPlayerDied(this);
            }
        }
    }
}
