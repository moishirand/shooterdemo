﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed;
    public int health;
    public int damage;
    public Transform playerTransform;

    void Update()
    {
        if (playerTransform != null)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, playerTransform.transform.position, Time.deltaTime * moveSpeed);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void Attack(Player player)
    {
        player.health -= this.damage;
        Destroy(gameObject);
    }

    public void Initialize(Transform target, float moveSpeed, int health)
    {
        this.playerTransform = target;
        this.moveSpeed = moveSpeed;
        this.health = health;
    }
}
